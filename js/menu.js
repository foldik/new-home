'use strict';

(function() {
    let menu = document.getElementById('menu'),
        menuButton = document.getElementById('menu-trianlge-button'),
        menuItems = document.querySelectorAll(".menu-items a")
    var placeToGoHash = null,
        yPositionCache = window.scrollY,
        scrollCallback = null

    menuButton.addEventListener('click', function() {
        if (!menuButton.classList.contains('hide')) {
            menu.classList.toggle('open')
        }
    })

    menuItems.forEach(function(menuItem) {
        menuItem.addEventListener('click', function(event) {
            event.preventDefault()
            event.stopPropagation()

            menu.classList.remove('open')
            placeToGoHash = event.target.hash
        })
    })

    menu.addEventListener('transitionend', function() {
        if (placeToGoHash) {
            let elementToGo = document.getElementById(placeToGoHash.replace('#', ''))
            window.scroll({
                top: elementToGo.offsetTop,
                left: 0,
                behavior: 'smooth'
            })
            menuButton.classList.add('hide')
        }
    })

    window.addEventListener('scroll', function() {
        menu.classList.remove('open')
        if (scrollCallback) {
            clearTimeout(scrollCallback)
            scrollCallback = null
        }
        scrollCallback = setTimeout(scrollHandler, 150)
    })

    function scrollHandler() {
        if (!menu.classList.contains('open')) {
            if (yPositionCache < window.scrollY) {
                menuButton.classList.add('hide')
            } else if (yPositionCache > window.scrollY && placeToGoHash === null) {
                menuButton.classList.remove('hide')
            }
            yPositionCache = window.scrollY
        }
        placeToGoHash = null
    }
}())